from django.apps import AppConfig


class MyReferralLinksConfig(AppConfig):
    name = 'my_referral_links'
